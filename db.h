#ifndef DB_H
#define DB_H
#include <stdint.h>
#include <list>
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>

#define VERIFY "DBCBAVCW"
#define FILE_NAME "list.bin"

typedef struct Date{
	unsigned tm_mday;
	unsigned tm_mon;
	unsigned tm_year;
}Date;

#define MAX_BYTE_STORE 128
#define MAX_BYTE_STORE_GROUP 64


typedef struct{
	char firstName[MAX_BYTE_STORE];
	char lastName[MAX_BYTE_STORE];
	char middleName[MAX_BYTE_STORE];
	char group[MAX_BYTE_STORE_GROUP];
	Date birthday;
	unsigned long long number;
	int course;
}Student;
typedef bool (*CompareFunc)(const Student&, const Student&);
extern CompareFunc comp_funcs_in[];
extern CompareFunc comp_funcs_de[];
typedef struct FileHeaderTable{
	//fpos_t fperson_pos;
	char verify[32];
	uint64_t fperson_pos;
	uint64_t person_num;
}FileHeaderTable;

namespace DataBase{
	extern std::string filename;
	extern std::fstream file;
	extern struct FileHeaderTable mFHT;
	extern std::list<Student> Stds;

}

int openDataBase();
int saveDataBase();

#endif // DB_H
