#define _WIN32_IE 0x0300
#include <windows.h> // заголовочный файл, содержащий WINAPI
#include <commctrl.h>
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <locale>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include "db.h"


using namespace std;

#define MENU_OPEN 1001
#define MENU_SAVE 1002
#define MENU_EXIT 1003

#define MENU_ADD 1004
#define MENU_CHANGE 1005
#define MENU_REMOVE 1006


#define ID_LISTVIEW 3001


#define ASD_ADD_ST 3111
#define ASD_CHANGE_ST 3112
#define WM_ASD_RES 3113




// Прототип функции обработки сообщений с пользовательским названием:
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK AddWndProc(HWND, UINT, WPARAM, LPARAM);
ATOM createMainWindowClass(HINSTANCE, WNDPROC, LPCTSTR);
HWND hMainWnd;
HINSTANCE hInst;
TCHAR szAddSt[] = "AddWin";
string narr[]={"First name", "Last name", "Middle name", "Group","Number", "Bithday"};

void createListView(HWND listView);
void readStudents(HWND listView, list<Student> l);
void addItemToLv(HWND listView, Student st, int i);


int WINAPI WinMain(HINSTANCE hInst, // дескриптор экземпляра приложения
                   HINSTANCE hPrevInst, // не используем
                   LPSTR lpCmdLine, // не используем
                   int nCmdShow) // режим отображения окошка
{
	::hInst = hInst;
	TCHAR szClassName[] = "WinClass";
	srand(time(NULL));
	openDataBase();


    if(!createMainWindowClass(hInst, WndProc, szClassName))
		return -1;
	if(!createMainWindowClass(hInst, AddWndProc, szAddSt))
		return -1;

    hMainWnd = CreateWindow(
        szClassName, "Program (V1)", WS_OVERLAPPED| WS_CAPTION| WS_SYSMENU| WS_MINIMIZEBOX,
	//	WS_BORDER|WS_CAPTION, // режимы отображения окошка
        CW_USEDEFAULT, CW_USEDEFAULT, 700, 620,
        (HWND)NULL, NULL, HINSTANCE(hInst), NULL);
    if(!hMainWnd){
        return NULL;
    }
	ShowWindow(hMainWnd, nCmdShow);
    UpdateWindow(hMainWnd); // обновляем окошко
	MSG msg;
    while(GetMessage(&msg, NULL, NULL, NULL)){
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return msg.wParam;
}
ATOM createMainWindowClass(HINSTANCE hInst, WNDPROC WndProc, LPCTSTR szClassName){
	WNDCLASSEX wc; // создаём экземпляр, для обращения к членам класса WNDCLASSEX
    wc.cbSize        = sizeof(wc); // размер структуры (в байтах)
    wc.style         = CS_HREDRAW | CS_VREDRAW; // стиль класса окошка
    wc.lpfnWndProc   = WndProc; // указатель на пользовательскую функцию
    wc.lpszMenuName  = NULL; // указатель на имя меню (у нас его нет)
    wc.lpszClassName = szClassName; // указатель на имя класса
    wc.cbWndExtra    = NULL; // число освобождаемых байтов в конце структуры
    wc.cbClsExtra    = NULL; // число освобождаемых байтов при создании экземпляра приложения
    wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO); // декриптор пиктограммы
    wc.hIconSm       = LoadIcon(NULL, IDI_WINLOGO); // дескриптор маленькой пиктограммы (в трэе)
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW); // дескриптор курсора
    wc.hbrBackground = CreateSolidBrush(RGB(255, 250, 250));//(HBRUSH)(COLOR_WINDOW+1);
   // wc.hbrBackground = CreateSolidBrush ( RGB ( rand()%256, rand()%256, rand()%256));//(HBRUSH)GetStockObject(WHITE_BRUSH); // дескриптор кисти для закраски фона окна
    wc.hInstance     = hInst; // указатель на строку, содержащую имя меню, применяемого для класса
	return RegisterClassEx(&wc);
}

void createListView(HWND listView){
	LV_COLUMN LvCol;
	memset(&LvCol,0,sizeof(LvCol));                  // Zero Members
	LvCol.mask=LVCF_TEXT|LVCF_WIDTH|LVCF_SUBITEM;    // Type of mask
	LvCol.cx=0x28;                                   // width between each coloum                          // First Header Text
	LvCol.cx=0x42;
	LvCol.cx=0x64;
							   // width of column

			SendMessage(listView,LVM_SETEXTENDEDLISTVIEWSTYLE,
					0,LVS_EX_FULLROWSELECT);
	for(int i = 0; i < 6; i++){
		LvCol.pszText=(char*)narr[i].c_str();
		SendMessage(listView,LVM_INSERTCOLUMN,i,(LPARAM)&LvCol);
	}
}
void readStudents(HWND listView, list<Student> l){
	SendMessage(listView,LVM_DELETEALLITEMS,0,0);
	list<Student>::iterator it;
	int i = 0;
	for(it = l.begin(); it!=l.end(); it++){
		addItemToLv(listView, *it, i);
		i++;
	}
}
void addItemToLv(HWND listView, Student st, int i){
	LV_ITEM LvItem;
	char tmp[256];

	LvItem.mask=LVIF_TEXT;   // Text Style
	LvItem.cchTextMax = 256; // Max size of test
	LvItem.iItem=i;          // choose item
	LvItem.iSubItem=0;       // Put in first coluom
	sprintf(tmp, "%s", st.firstName);
	LvItem.pszText=tmp;
	SendMessage(listView,LVM_INSERTITEM,0,(LPARAM)&LvItem);

	sprintf(tmp, "%s", st.lastName);
	LvItem.iSubItem++;
	SendMessage(listView,LVM_SETITEM,0,(LPARAM)&LvItem);
	sprintf(tmp, "%s", st.middleName);
	LvItem.iSubItem++;
	SendMessage(listView,LVM_SETITEM,0,(LPARAM)&LvItem);
	sprintf(tmp, "%s", st.group);
	LvItem.iSubItem++;
	SendMessage(listView,LVM_SETITEM,0,(LPARAM)&LvItem);
//	sprintf(tmp, "%d", st.course);
//	LvItem.iSubItem++;
	SendMessage(listView,LVM_SETITEM,0,(LPARAM)&LvItem);
	sprintf(tmp, "%llu", st.number);
	//sprintf(tmp, "%02d.%02d.%d", st.birthday.tm_mday,st.birthday.tm_mon, st.birthday.tm_year);
	LvItem.iSubItem++;
	SendMessage(listView,LVM_SETITEM,0,(LPARAM)&LvItem);
	sprintf(tmp, "%02d.%02d.%d", st.birthday.tm_mday,st.birthday.tm_mon, st.birthday.tm_year);
	LvItem.iSubItem++;
	SendMessage(listView,LVM_SETITEM,0,(LPARAM)&LvItem);

}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
	static HMENU mMenu, mMenuFile, popupMenu, mSortMenu, mIMenu, mDMenu;
	static HWND listView, AddDialog;
	static LV_COLUMN LvCol;
	static LV_ITEM LvItem;

	char Temp[255];
	MINMAXINFO* inf;
	POINT pos;
	int r;
	static int iSel = -1;
    switch(uMsg){
		case WM_CREATE:
			mMenu = CreateMenu();
			mMenuFile = CreatePopupMenu();
			mSortMenu = CreatePopupMenu();
			AppendMenu(mMenuFile, MF_STRING, MENU_OPEN,"Open");
			AppendMenu(mMenuFile, MF_STRING, MENU_SAVE,"Save");
			AppendMenu(mMenuFile, MF_STRING, 0,0);
			AppendMenu(mMenuFile, MF_STRING, MENU_EXIT,"Exit");
			AppendMenu(mMenu, MF_STRING|MF_POPUP, (UINT)mMenuFile,"FILE");
			mIMenu = CreatePopupMenu();
			mDMenu = CreatePopupMenu();
			int i;

			for(i = 0; i <6; i++ )
				AppendMenu(mIMenu, MF_STRING, i+4301,narr[i].c_str() );
			for(i = 0; i <6; i++ )
				AppendMenu(mDMenu, MF_STRING, i+4401,narr[i].c_str());

			AppendMenu(mSortMenu, MF_STRING|MF_POPUP, (UINT)mIMenu,"Increase");
			AppendMenu(mSortMenu, MF_STRING|MF_POPUP, (UINT)mDMenu,"Decrease");
			AppendMenu(mMenu, MF_STRING|MF_POPUP, (UINT)mSortMenu,"Sorting");

			SetMenu(hWnd, mMenu);

			popupMenu = CreatePopupMenu();
			AppendMenu(popupMenu, MF_STRING, MENU_ADD, "Add");
			AppendMenu(popupMenu, MF_STRING, MENU_CHANGE, "Change");
			AppendMenu(popupMenu, MF_STRING, MENU_REMOVE, "Remove");

			listView = CreateWindow(
				WC_LISTVIEW, "", WS_VISIBLE|WS_BORDER|WS_CHILD | LVS_REPORT,// | LVS_EDITLABELS,
			//	WS_BORDER|WS_CAPTION, // режимы отображения окошка
				10, 10, 670, 550,
				(HWND)hWnd, (HMENU) ID_LISTVIEW, HINSTANCE(hInst), NULL);

			createListView(listView);
			readStudents(listView, DataBase::Stds);

			AddDialog = CreateWindowEx(WS_EX_TOPMOST,
			szAddSt, "Add", WS_OVERLAPPED| WS_CAPTION| WS_SYSMENU| WS_MINIMIZEBOX,
	//	WS_BORDER|WS_CAPTION, // режимы отображения окошка
			CW_USEDEFAULT, CW_USEDEFAULT, 290, 250,
			(HWND)NULL, NULL, HINSTANCE(hInst), NULL);
			break;

		case WM_NOTIFY:
			printf("1\n");
			switch(LOWORD(wParam)){
				case ID_LISTVIEW:
					if(((LPNMHDR)lParam)->code == NM_RCLICK)
					{
						printf("1\n");
						GetCursorPos(&pos);
						TrackPopupMenu(popupMenu, TPM_LEFTBUTTON,
						pos.x,pos.y,0, hWnd,NULL);
						int iSlected=0;
						int j=0;

						iSlected=SendMessage(listView,LVM_GETNEXTITEM,-1,LVNI_FOCUSED);

						if(iSlected==-1)
						{


							break;
						}
						printf("sel %d\n", iSlected);
					}
					break;
			}
		break;
		case WM_GETMINMAXINFO:
			inf = (MINMAXINFO*) lParam;
			inf->ptMaxTrackSize.x =700;
			inf->ptMaxTrackSize.y = 620;
			inf->ptMinTrackSize = inf->ptMaxTrackSize;
		break;
		case WM_ASD_RES:
			printf("tfh\n");
			if(wParam!=-1&&lParam!=NULL){
				if(iSel>=0){
					int i = 0;
					list<Student>::iterator it;
					for(it = DataBase::Stds.begin();
					it!=DataBase::Stds.end()&&i<iSel; it++, i++);
					*it = *((Student*)lParam);
					readStudents(listView, DataBase::Stds);
					printf("i = %d\n", i);
				}else{
					addItemToLv(listView, *((Student*)lParam), 0);
					DataBase::Stds.push_front(*((Student*)lParam));
				}
			}
			EnableWindow(hWnd, TRUE);
		break;
		case WM_COMMAND:
			switch(LOWORD(wParam)){

				case ID_LISTVIEW:
					printf("1\n");
					break;

				case MENU_ADD:
					iSel = -1;
				//	DialogBox(hInst, szAddSt, hWnd, NULL);
					SendMessage(AddDialog, ASD_ADD_ST, 0, (LPARAM)hWnd);
					EnableWindow(hWnd, FALSE);
					break;
				case MENU_CHANGE:
					iSel = ListView_GetNextItem(listView, -1, LVNI_SELECTED);
				//	SendMessage(AddDialog, ASD_ADD_ST, (WPARAM)&st, 0L)
					if(iSel>=0){
						int i = 0;
						list<Student>::iterator it;
						for(it = DataBase::Stds.begin();
						it!=DataBase::Stds.end()&&i<iSel; it++, i++);
						SendMessage(AddDialog, ASD_CHANGE_ST, (WPARAM)&(*it), (LPARAM)hWnd);

						EnableWindow(hWnd, FALSE);
					}
					break;
				case MENU_REMOVE:
					iSel = -1;
					r = 0;
					if(MessageBox(hWnd,"You really want to remove the selected item(s)",0, MB_ICONWARNING|MB_YESNO|MB_DEFBUTTON2)==IDYES)
					while((iSel = ListView_GetNextItem(listView, iSel, LVNI_SELECTED))!=-1){
				//	SendMessage(AddDialog, ASD_ADD_ST, (WPARAM)&st, 0L)
						int i = 0;
						list<Student>::iterator it;
						for(it = DataBase::Stds.begin();
						it!=DataBase::Stds.end()&&i<iSel-r; it++, i++);
						DataBase::Stds.erase(it);
						r++;
						cout << "rem " << iSel << endl;

					}
					readStudents(listView, DataBase::Stds);
					break;
				case MENU_OPEN:
					openDataBase();
					readStudents(listView, DataBase::Stds);
					break;

				case MENU_SAVE:
					saveDataBase();
					break;
				case MENU_EXIT:
					PostQuitMessage(NULL);
				break;
				default:
					if(LOWORD(wParam)/1000 == 4){
						int s = LOWORD(wParam)%100;
						int di = LOWORD(wParam)/100%10;
						if(di == 3){
							DataBase::Stds.sort(comp_funcs_in[s-1]);
						}else if(di == 4){
							DataBase::Stds.sort(comp_funcs_de[s-1]);
						}
						readStudents(listView, DataBase::Stds);
					}
					break;
			}
	//		printf("%d %d %d %d\n", HIWORD(wParam), LOWORD(wParam), HIWORD(lParam), LOWORD(lParam));
		break;
    case WM_DESTROY: // если окошко закрылось, то:
        PostQuitMessage(NULL); // отправляем WinMain() сообщение WM_QUIT
        break;
    default:
        return DefWindowProc(hWnd, uMsg, wParam, lParam); // если закрыли окошко
    }
    return NULL; // возвращаем значение
}
#define ID_EDIT_FN 3000
#define ID_EDIT_LN 3001
#define ID_EDIT_MN 3002
#define ID_EDIT_GP 3003
#define ID_EDIT_NR 3004
#define ID_EDIT_CS 3005
#define ID_OK_BTN 3006
#define ID_CANCEL_BTN 3007


int isgroups(int c){
	return isalpha(c)||isdigit(c)||(c=='-');
}

LRESULT CALLBACK AddWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
	static HWND fstNmEn, lstNmEn, mdlNmEn, grpEn, crsEN, nbrEN, okBtn, clBtn, dtSL;
	static bool lock = false;
	char buf[256];
	int change = false;
	static Student st;
	static bool ff[7] = {false};
	static HWND parent;
	switch(uMsg){
		case WM_CREATE:
			CreateWindow("static","first name",BS_TEXT|WS_CHILD|WS_VISIBLE, 10, 30, 100, 20,
				hWnd, (HMENU)NULL, hInst, NULL);
			fstNmEn = CreateWindow("EDIT","",BS_TEXT|WS_CHILD|WS_VISIBLE|WS_BORDER, 120, 30, 150, 20,
				hWnd, (HMENU)ID_EDIT_FN, hInst, NULL);

			CreateWindow("static","last name",BS_TEXT|WS_CHILD|WS_VISIBLE, 10, 53, 100, 20,
				hWnd, (HMENU)NULL, hInst, NULL);
			lstNmEn = CreateWindow("EDIT","",BS_TEXT|WS_CHILD|WS_VISIBLE|WS_BORDER, 120, 53, 150, 20,
				hWnd, (HMENU)ID_EDIT_LN, hInst, NULL);

			CreateWindow("static","middle name",BS_TEXT|WS_CHILD|WS_VISIBLE, 10, 76, 100, 20,
				hWnd, (HMENU)NULL, hInst, NULL);
			mdlNmEn = CreateWindow("EDIT","",BS_TEXT|WS_CHILD|WS_VISIBLE|WS_BORDER, 120, 76, 150, 20,
				hWnd, (HMENU)ID_EDIT_MN, hInst, NULL);

			CreateWindow("static","group",BS_TEXT|WS_CHILD|WS_VISIBLE, 10, 99, 100, 20,
				hWnd, (HMENU)NULL, hInst, NULL);
			grpEn = CreateWindow("EDIT","",BS_TEXT|WS_CHILD|WS_VISIBLE|WS_BORDER|ES_UPPERCASE, 120, 99, 150, 20,
				hWnd, (HMENU)ID_EDIT_GP, hInst, NULL);

//			CreateWindow("static","course",BS_TEXT|WS_CHILD|WS_VISIBLE, 10, 122, 100, 20,
//				hWnd, (HMENU)NULL, hInst, NULL);
//			crsEN = CreateWindow("EDIT","",BS_TEXT|WS_CHILD|WS_VISIBLE|WS_BORDER, 120, 122, 150, 20,
//				hWnd, (HMENU)ID_EDIT_CS, hInst, NULL);

			CreateWindow("static","number",BS_TEXT|WS_CHILD|WS_VISIBLE, 10, 122, 100, 20,
				hWnd, (HMENU)NULL, hInst, NULL);
			nbrEN = CreateWindow("EDIT","",BS_TEXT|WS_CHILD|WS_VISIBLE|WS_BORDER, 120, 122, 150, 20,
				hWnd, (HMENU)ID_EDIT_NR, hInst, NULL);
			CreateWindow("static","birthday",BS_TEXT|WS_CHILD|WS_VISIBLE, 10, 145, 100, 20,
				hWnd, (HMENU)NULL, hInst, NULL);
			dtSL = CreateWindow(DATETIMEPICK_CLASS, TEXT("DateTime"),WS_BORDER|WS_CHILD|WS_VISIBLE,
                             120,145,150,20, hWnd, NULL, hInst, NULL);

			okBtn = CreateWindow("BUTTON","OK",BS_TEXT|WS_CHILD|WS_VISIBLE|WS_BORDER, 10, 168, 126, 20,
				hWnd, (HMENU)ID_OK_BTN, hInst, NULL);
			clBtn = CreateWindow("BUTTON","Cancel",BS_TEXT|WS_CHILD|WS_VISIBLE|WS_BORDER, 145, 168, 126, 20,
				hWnd, (HMENU)ID_CANCEL_BTN, hInst, NULL);

			SYSTEMTIME a[2];
			GetSystemTime(&a[1]);
			GetSystemTime(&a[0]);
			a[0].wYear = 1900;
			DateTime_SetRange(dtSL, GDTR_MIN, a);
			DateTime_SetSystemtime(dtSL, GDT_NONE, &a[0]);

		break;
		case WM_COMMAND:

			switch(LOWORD(wParam)){
				case ID_OK_BTN:
				//	if(HIWORD(wParam)==1){
				//		if(st!=NULL){
						GetWindowText(fstNmEn, buf, GetWindowTextLength(fstNmEn)+1);
						strcpy(st.firstName, buf);
						GetWindowText(lstNmEn, buf, GetWindowTextLength(lstNmEn)+1);
						strcpy(st.lastName, buf);
						GetWindowText(mdlNmEn, buf, GetWindowTextLength(mdlNmEn)+1);
						strcpy(st.middleName, buf);
						GetWindowText(grpEn, buf, GetWindowTextLength(grpEn)+1);
						strcpy(st.group, buf);
						GetWindowText(crsEN, buf, GetWindowTextLength(crsEN)+1);
						sscanf(buf, "%d", &st.course );
						GetWindowText(nbrEN, buf, GetWindowTextLength(nbrEN)+1);
						sscanf(buf, "%llu", &st.number );
						SYSTEMTIME date;
						DateTime_GetSystemtime(dtSL, &date);
				//		}
						st.birthday.tm_year = date.wYear;
						st.birthday.tm_mon = date.wMonth;
						st.birthday.tm_mday = date.wDay;
						SendMessage(parent, WM_ASD_RES, 0, (LPARAM)&st );
						ShowWindow(hWnd, FALSE);
				//	}
				break;
				case ID_CANCEL_BTN:
					SendMessage(parent, WM_ASD_RES, -1, 0L );

					ShowWindow(hWnd, FALSE);
				break;
				default:
					switch(HIWORD(wParam)){
						case EN_CHANGE:
							HWND ed;
							int (*func)(int);
							int m = 1;
							int min = 1;
							bool b = false;
							switch(LOWORD(wParam)){
								case ID_EDIT_FN:
									ed = fstNmEn;
									func = isalpha;
									m = 15;
								break;
								case ID_EDIT_LN:
									ed = lstNmEn;
									func = isalpha;
									m = 15;
								break;
								case ID_EDIT_MN:
									ed = mdlNmEn;
									func = isalpha;
									m = 15;
								break;
								case ID_EDIT_GP:
									ed = grpEn;
									func = isgroups;
									m = 10;
								break;
/*								case ID_EDIT_CS:
									ed = crsEN;
									func = isdigit;
									m = 1;
						//			printf("cs\n");
								break;
*/
								case ID_EDIT_NR:
									ed = nbrEN;
									func = isdigit;
									m = 10;
									min = 10;
								break;
							}
							if(!lock){
								lock = true;
								char buf[256];
								int i, j, len, c;
								SendMessage(ed, EM_GETSEL, (WPARAM) &c, 0);
								len = GetWindowTextLength(ed)+1;
								GetWindowText(ed, buf, len);
					//			printf("cs %s |\n", buf);
								j = 0;
								for(i = 0; (i < len)&&(j<m); i++){
									if(func(buf[i])){
										buf[j++] = buf[i];
									}
								}

								ff[LOWORD(wParam)-3000] = (j>=min);

								buf[j]=0;
								SetWindowText(ed, buf);
								SendMessage(ed, EM_SETSEL, (WPARAM) c, (LPARAM)c);
								lock = false;
							}
							b = true;
							for(int i = 0; i < 5; i++){
								cout << "ff "<<ff[i] << endl;
								if(!ff[i]){
									b = false;
									break;
								}
							}
							cout << endl;
							EnableWindow(okBtn, b);
						break;
					}
				break;


			}

			break;

		case ASD_ADD_ST:
			parent = (HWND)lParam;
			SetWindowText(fstNmEn, "");
			SetWindowText(lstNmEn, "");
			SetWindowText(mdlNmEn, "");
			SetWindowText(grpEn, "");
//			SetWindowText(crsEN, "");
			SetWindowText(nbrEN, "");
			ShowWindow(hWnd, TRUE);
		break;
		case ASD_CHANGE_ST:
			parent = (HWND)lParam;
			st = *((Student*)wParam);
			SetWindowText(fstNmEn, st.firstName);
			SetWindowText(lstNmEn, st.lastName);
			SetWindowText(mdlNmEn, st.middleName);

			SetWindowText(grpEn, st.group);
			SYSTEMTIME date;
			date.wYear = st.birthday.tm_year;
			date.wMonth = st.birthday.tm_mon;
			date.wDay = st.birthday.tm_mday;
			DateTime_SetSystemtime(dtSL, GDT_NONE, &date);

			char buf[64];
//			sprintf(buf, "%d", st.course);
//			SetWindowText(crsEN, buf);
			sprintf(buf, "%llu", st.number);
			SetWindowText(nbrEN, buf);
			ShowWindow(hWnd, TRUE);
		break;
		case WM_CLOSE:
			SendMessage(parent, WM_ASD_RES, -1, 0L );
			ShowWindow(hWnd, FALSE);
		break;
		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
}
