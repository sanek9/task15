#include "db.h"
#include <fstream>

namespace DataBase{
	std::string filename;
	std::fstream file;
	struct FileHeaderTable mFHT;
	std::list<Student> Stds;

}

bool compare_nocase (const std::string& first, const std::string& second)
{
  unsigned int i=0;
  while ( (i<first.length()) && (i<second.length()) )
  {
    if (tolower(first[i])<tolower(second[i])) return true;
    else if (tolower(first[i])>tolower(second[i])) return false;
    ++i;
  }
  return ( first.length() < second.length() );
}
bool compare_first_name (const Student& st1, const Student& st2)
{
	std::string s1(st1.firstName);
	std::string s2(st2.firstName);
	return compare_nocase(s1, s2);
}
bool compare_last_name (const Student& st1, const Student& st2)
{
	std::string s1(st1.lastName);
	std::string s2(st2.lastName);
	return compare_nocase(s1, s2);
}
bool compare_middle_name (const Student& st1, const Student& st2)
{
	std::string s1(st1.middleName);
	std::string s2(st2.middleName);
	return compare_nocase(s1, s2);
}
bool compare_group (const Student& st1, const Student& st2)
{
	std::string s1(st1.group);
	std::string s2(st2.group);
	return compare_nocase(s1, s2);
}
bool compare_birthday (const Student& st1, const Student& st2)
{
	if(st1.birthday.tm_year<st2.birthday.tm_year)
		return true;
	else if(st1.birthday.tm_year>st2.birthday.tm_year)
		return false;
	else if(st1.birthday.tm_year<st2.birthday.tm_year)
		return true;
	else if(st1.birthday.tm_mon>st2.birthday.tm_mon)
		return false;
	else if(st1.birthday.tm_mon<st2.birthday.tm_mon)
		return true;
	else if(st1.birthday.tm_mday>st2.birthday.tm_mday)
		return false;
	return true;
}
bool compare_number (const Student& st1, const Student& st2)
{
	return st1.number < st2.number;
}

CompareFunc comp_funcs_in[] = {compare_first_name,compare_last_name, compare_middle_name,
							compare_group, compare_number, compare_birthday};
bool compare_first_name_dec (const Student& st1, const Student& st2)
{
	return !compare_first_name(st1, st2);
}
bool compare_last_name_dec (const Student& st1, const Student& st2)
{
	return !compare_last_name(st1, st2);
}
bool compare_middle_name_dec (const Student& st1, const Student& st2)
{
	return !compare_middle_name(st1, st2);
}
bool compare_group_dec (const Student& st1, const Student& st2)
{
	return !compare_group(st1, st2);
}
bool compare_birthday_dec (const Student& st1, const Student& st2)
{
	return !compare_birthday(st1, st2);
}
bool compare_number_dec (const Student& st1, const Student& st2)
{
	return !compare_number(st1, st2);
}

CompareFunc comp_funcs_de[] = {compare_first_name_dec,compare_last_name_dec, compare_middle_name_dec,
							compare_group_dec, compare_number_dec, compare_birthday_dec};
using namespace DataBase;
int openDataBase(){
	std::ifstream file;
	file.open(FILE_NAME, std::fstream::in | std::fstream::binary);
	if(file.is_open()){
		DataBase::Stds.clear();
		while(!file.eof()){
			Student s;
			file.read((char*) &s, sizeof(Student));
			if((file.rdstate()&& std::ifstream::eofbit)!=0){
				break;
			}
	//		std::cout << s.lastName << std::endl;
			DataBase::Stds.push_back(s);
		}
		file.close();
	}else{
		printf("1\n");
		return -1;
	}

	return 0;
}
int saveDataBase(){
	std::ofstream file;
	file.open(FILE_NAME, std::fstream::out | std::fstream::binary);
	if(file.is_open()){
		for(std::list< Student >::iterator i = Stds.begin(); i != Stds.end(); i++){
			file.write((char*)&(*i), sizeof(Student));
			std::cout << i->firstName << std::endl;
		}
		file.close();
	}else{
		printf("1\n");
		return -1;
	}

	return 0;
}
